#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <fstream>
using namespace sf;
using namespace std;
int score=0;


template<typename TYPE>
void CoutBackground(TYPE &window)
{
    Texture BACKGROUNDtexture; //for background
	Sprite background;
	BACKGROUNDtexture.loadFromFile("pictures/background.jpg");
	background.setTexture(BACKGROUNDtexture);
	background.setPosition(0,0);
    window.draw(background);
}

template<typename TYPE, int row, int col>
void CoutTile(char (&area)[row][col],TYPE &window)
{
    Texture tile; //for tile
    tile.loadFromFile("pictures/tile.png");
	Sprite TILEsprite;
	TILEsprite.setTexture(tile);

    for (int i=0;i<area_y;i++)
        for(int j=0;j<area_x;j++)
        {
            if(area[i][j]=='1')
            TILEsprite.setTextureRect(IntRect(0,0,20,20));
            else if(area[i][j]=='2')
            TILEsprite.setTextureRect(IntRect(20,0,20,20));
            else if(area[i][j]=='3')
            TILEsprite.setTextureRect(IntRect(40,0,20,20));
            else if(area[i][j]=='4')
            TILEsprite.setTextureRect(IntRect(60,0,20,20));
            else if(area[i][j]=='5')
            TILEsprite.setTextureRect(IntRect(80,0,20,20));
            else if(area[i][j]=='6')
            TILEsprite.setTextureRect(IntRect(100,0,20,20));
            else if(area[i][j]=='7')
            TILEsprite.setTextureRect(IntRect(120,0,20,20));
            else
            TILEsprite.setTextureRect(IntRect(0,0,0,0));

            TILEsprite.setPosition(j*20+89,i*20+20);
            window.draw(TILEsprite);
        }
}

template< typename TYPE>
void GuiCoutText(char *name,int &value,TYPE &window,int y,int x,int collor_choose)
{
    sf::Font font;
    font.loadFromFile("arial.ttf");
    sf::Text text;
    text.setFont(font); // font is a sf::Font
    char buffer[10];
    sprintf(buffer,"%s%d",name,value);
    text.setString(buffer);
    text.setCharacterSize(24); // in pixels, not points!
    if(collor_choose==2)
    text.setColor(sf::Color::White);
    if(collor_choose==1)
    text.setColor(sf::Color::Red);
    text.setPosition(y,x);
    text.setStyle(sf::Text::Bold | sf::Text::Underlined);
    window.draw(text);
}

template< typename TYPE, int row, int col>
void GuiCout(char (&area)[row][col],TYPE &window)
{
    CoutBackground(window);
    CoutTile(area,window);
    GuiCoutText("SCORE: ",score,window,190,0,1);
}

template< typename TYPE>
void GuiMenuCout(int menu_choose, TYPE &window)
{
        Texture play,high_scores,quit,high_score_show;
        Sprite playy,high_scoree,quitt,high_score_showw;
        play.loadFromFile("pictures/menu_play.png");
        high_scores.loadFromFile("pictures/menu_high_scores.png");
        quit.loadFromFile("pictures/menu_quit.png");
        high_score_show.loadFromFile("pictures/high_score_show.png");
        playy.setTexture(play);
        high_scoree.setTexture(high_scores);
        quitt.setTexture(quit);
        high_score_showw.setTexture(high_score_show);
        playy.setPosition(0,0);
        high_scoree.setPosition(0,0);
        quitt.setPosition(0,0);
        high_score_showw.setPosition(0,0);

            if(menu_choose == 0)
            {
                //window.clear();
                window.draw(playy);
            }
            else if (menu_choose == 1)
            {
               // window.clear();
                window.draw(high_scoree);
            }
            else if (menu_choose == 2)
            {
                //window.clear();
                window.draw(quitt);
            }
            else if (menu_choose == 4)
            {
                window.draw(high_score_showw);
                ///
               ifstream file("score.txt");
               while(file)
               {
                int str;
                char buffer[10];
                file >> str;
                GuiCoutText("",str,window,190,212,2);
                file.close();
               }
                ///
            }
            window.display();
}
