#include <iostream>
#include <sys/time.h>
#include <time.h>
#include <SFML/System.hpp>
#include "tile.h"
#include "tile_reverse.h"
#include "windows.h"
//#include "GuiFunc.h"
#define area_y 21
#define area_x 10
#define tile_x 4
#define tile_y 4

using namespace std;

template <int row, int col>
void CoutArea(char (&area)[row][col])
{
    /*
        for (int i=0; i<row;i++)
        {
            for (int j=0; j<col; j++)
            {
                cout<<area[i][j];
            }
            cout<<endl;
        }
        */
}

template <int row, int col>
char IniArea(char (&area)[row][col])
{
    for (int i=0; i<row;i++)
        for (int j=0; j<col; j++)
            area[i][j]='0';
}

template <int row, int col>
void IncreaseArea(char (&area)[row][col],int y,int x)
{
    for (int i=0;i<tile_y;i++)
        for(int j=0;j<tile_x;j++)
            if (maze_fucker[i][j] != '0')
                area[y+i][x+j] = maze_fucker[i][j];
}

template <int row, int col>
bool CheckBarrierDown(char (&area)[row][col],int y,int x)
{
    for (int i=0;i<tile_y;i++)
        for(int j=0;j<tile_x;j++)
            if (maze_fucker[i][j] != '0')
                if (area[y+i+1][x+j] != '0') return false;

    return true;
}

template <int row, int col>
bool CheckBarrierLeftRight(int left_right,char (&area)[row][col],int y,int x)
{
    switch (left_right)
    {
        case 1: //left

            for (int i=0;i<tile_y;i++)
                for(int j=0;j<tile_x;j++)
                    if (maze_fucker[i][j] != '0')
                        if (area[y+i][x+j-1] == '1' ||
                            area[y+i][x+j-1] == '2' ||
                            area[y+i][x+j-1] == '3' ||
                            area[y+i][x+j-1] == '4' ||
                            area[y+i][x+j-1] == '5' ||
                            area[y+i][x+j-1] == '6' ||
                            area[y+i][x+j-1] == '7' ) return false;

            return true;
            break;

            case 2: //right

            for (int i=0;i<tile_y;i++)
                for(int j=0;j<tile_x;j++)
                    if (maze_fucker[i][j] != '0')
                        if (area[y+i][x+j+1] == '1' ||
                            area[y+i][x+j+1] == '2' ||
                            area[y+i][x+j+1] == '3' ||
                            area[y+i][x+j+1] == '4' ||
                            area[y+i][x+j+1] == '5' ||
                            area[y+i][x+j+1] == '6' ||
                            area[y+i][x+j+1] == '7' ) return false;

            return true;
            break;

        default:
            cout<<"bad left_right value";

        return 99;
        break;
    }
}


template <int row1, int col1,int row2, int col2>
char CopyCleanArea(char (&new_area)[row1][col1],char (&old_area)[row2][col2])
{
    for (int i=0; i<row1;i++)
        for (int j=0; j<col1; j++)
            new_area[i][j]=old_area[i][j];
}


int GetTime(time_t &timer,time_t &tmp_timer, int delay)
{
    timer = GetTickCount();
    tmp_timer=timer+delay;
}

int GetTime(time_t &timer)
{
    timer = GetTickCount();
}
void MYdelay(int delay_value)
{
    time_t timer, tmp_timer;
    GetTime(timer,tmp_timer,delay_value);

    while(timer<tmp_timer)
    {
        GetTime(timer);
    }
}

int CountTileYElements()
{
    int tmp=0;

    for(int i=0;i<tile_y;i++)
            for (int j=0;j<tile_x;j++)
                if(maze_fucker[i][j] != '0')
                    if(j>=tmp)
                        tmp=j;
    return tmp+1;
}


template <int row1, int col1,int row2, int col2>
int SetNewTile(char (&maze_fucker)[row1][col1],char (&new_tile)[row2][col2])
{
    for (int i=0;i<tile_y;i++)
        for(int j=0;j<tile_x;j++)
                maze_fucker[i][j] = new_tile[i][j];
}


int GenerateNewTile()
{
    srand (time(NULL)); //fica ot Tomasa :D
                        //stob nekesirovalos
    Tile_choose=rand()%7+1;;

    switch (Tile_choose)
    {
        case 1:
            SetNewTile(maze_fucker,tile_1_1x);
        break;

        case 2:
            SetNewTile(maze_fucker,tile_2_1x);
        break;

        case 3:
            SetNewTile(maze_fucker,tile_3_1x);
        break;

        case 4:
            SetNewTile(maze_fucker,tile_4_1x);
        break;

        case 5:
            SetNewTile(maze_fucker,tile_5_1x);
        break;

        case 6:
            SetNewTile(maze_fucker,tile_6_1x);
        break;

        case 7:
            SetNewTile(maze_fucker,tile_7_1x);
        break;

        default:
            cout<<"rand() error, value: "<<Tile_choose<<endl;
        break;
    }
}

int RotateTile()
{
    switch (Tile_choose)
    {
        case 1:
            if (Tile_rotate == 8){
                SetNewTile(maze_fucker,tile_1_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 4){
                SetNewTile(maze_fucker,tile_1_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 2){
                SetNewTile(maze_fucker,tile_1_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 1){
                SetNewTile(maze_fucker,tile_1_1x); Tile_rotate =8;}

        break;

        case 2:
            if (Tile_rotate == 8){
                SetNewTile(maze_fucker,tile_2_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 4){
                SetNewTile(maze_fucker,tile_2_2x); Tile_rotate /=2;}
            else if (Tile_rotate == 2){
                SetNewTile(maze_fucker,tile_2_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 1){
                SetNewTile(maze_fucker,tile_2_2x);  Tile_rotate =8;}
        break;

        case 3:
            if (Tile_rotate == 8){
                SetNewTile(maze_fucker,tile_3_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 4){
                SetNewTile(maze_fucker,tile_3_2x); Tile_rotate /=2;}
            else if (Tile_rotate == 2){
                SetNewTile(maze_fucker,tile_3_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 1){
                SetNewTile(maze_fucker,tile_3_2x); Tile_rotate =8;}
        break;

        case 4:
            if (Tile_rotate == 8){
                SetNewTile(maze_fucker,tile_4_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 4){
                SetNewTile(maze_fucker,tile_4_2x); Tile_rotate /=2;}
            else if (Tile_rotate == 2){
                SetNewTile(maze_fucker,tile_4_3x); Tile_rotate /=2;}
            else if (Tile_rotate == 1){
                SetNewTile(maze_fucker,tile_4_4x); Tile_rotate =8;}
        break;

        case 5:
            if (Tile_rotate == 8){
                SetNewTile(maze_fucker,tile_5_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 4){
                SetNewTile(maze_fucker,tile_5_2x); Tile_rotate /=2;}
            else if (Tile_rotate == 2){
                SetNewTile(maze_fucker,tile_5_3x); Tile_rotate /=2;}
            else if (Tile_rotate == 1){
                SetNewTile(maze_fucker,tile_5_4x); Tile_rotate =8;}
        break;

        case 6:
            if (Tile_rotate == 8){
                SetNewTile(maze_fucker,tile_6_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 4){
                SetNewTile(maze_fucker,tile_6_2x); Tile_rotate /=2;}
            else if (Tile_rotate == 2){
                SetNewTile(maze_fucker,tile_6_3x); Tile_rotate /=2;}
            else if (Tile_rotate == 1){
                SetNewTile(maze_fucker,tile_6_4x); Tile_rotate =8;}
        break;

        case 7:
            if (Tile_rotate == 8){
                SetNewTile(maze_fucker,tile_7_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 4){
                SetNewTile(maze_fucker,tile_7_2x); Tile_rotate /=2;}
            else if (Tile_rotate == 2){
                SetNewTile(maze_fucker,tile_7_1x); Tile_rotate /=2;}
            else if (Tile_rotate == 1){
                SetNewTile(maze_fucker,tile_7_2x); Tile_rotate =8;}
        break;

        default:
            cout<<"Tile_choose error"<<endl;
        break;
    }
}


template <int row1, int col1,int row2, int col2>
void DeleteLine(int &score,char find_y_value,char (&new_area)[row1][col1],char (&old_area)[row2][col2])
{
    for (int i=0; i<area_x;i++)
        for (int j=0; j<find_y_value; j++)
            new_area[j+1][i]=old_area[j][i];

    score +=80; //total add 100
}

template <int row1, int col1,int row2, int col2>
int Find_and_DeleteLine(int &score,char (&new_area)[row1][col1],char (&old_area)[row2][col2])
{
    int delete_true=0;
    char tmp_sum=0;
    char find_y_value=0;
    for (int i=0; i<row2;i++)
        {
            for (int j=0; j<col2; j++)
            {
                if (old_area[i][j] != '0')
                    tmp_sum++;
                else
                    tmp_sum=0;
            }
            if (tmp_sum==area_x)
            {
                find_y_value=i;
                DeleteLine(score,find_y_value,new_area,old_area);
                delete_true=1;
            }
            tmp_sum=0;
        }

    return delete_true;
}

