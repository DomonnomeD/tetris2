#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <fstream>
#include "TetrisFunc.h"
#include "GuiFunc.h"
#include <conio.h> //for keyboard
#include <time.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>


#define area_y 21
#define area_x 10
using namespace std;
using namespace sf;

///global variables
char area[area_y][area_x];
char copyarea[area_y][area_x]; //using for final save area and tile pos,
                       //then tile is need to change
int y=0,x=0;
int Speed=300;




int main()
{
    sf::RenderWindow window(sf::VideoMode(360, 478), "tEtRiS!");
begin_game:

    Speed=300;
    sf::Music menu_music,game_over_music,game_music,block_music,line_remove;
    menu_music.openFromFile("sound/menu_music.wav");
    game_over_music.openFromFile("sound/gameover.wav");
    game_music.openFromFile("sound/game_music.wav");
    block_music.openFromFile("sound/block.wav");
    line_remove.openFromFile("sound/line-remove.wav");
    game_over_music.stop();


    score=0;
    IniArea(area);
    IniArea(copyarea); //save previous state
                       //with final area + tile
    GenerateNewTile();
    sf::Event event;

/*menu*/
            menu_music.play();
            menu_music.setLoop(1);
            int menu_choose=0; //play=0 high score=1 quit=2 //just for swaping
            while(1)
            {
                    while (window.pollEvent(event))
                    {
                        if (event.type == sf::Event::Closed)
                            window.close();
                    }
                    if (kbhit() || sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
                   {
                    char get_key=0;

                    if (kbhit()) //fiz infinity wait keyboard from cmd
                    get_key=getch();

                    if (get_key == 115 || sf::Keyboard::isKeyPressed(sf::Keyboard::S)) //down
                    {
                        if(menu_choose<2)
                            menu_choose++;
                        else
                            menu_choose=0;

                        MYdelay(50);
                    }
                    if (get_key == 119 || sf::Keyboard::isKeyPressed(sf::Keyboard::W)) //up
                    {
                        if(menu_choose>0)
                            menu_choose--;
                        else
                            menu_choose=2;

                        MYdelay(50);
                    }
                    if (get_key == 13 || sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) //enter
                    {
                        if(menu_choose == 0)
                        {
                            menu_music.stop();
                            game_music.play();
                            game_music.setLoop(true);
                            break; //go to game
                        }

                        else if(menu_choose == 1)
                            menu_choose=4;
                        else if(menu_choose == 2)
                            goto exit;

                        MYdelay(50);
                    }
                }
                GuiMenuCout(menu_choose,window);

                MYdelay(50);
            }

/*game window*/
            while (window.isOpen())
            {

                while (window.pollEvent(event))
                {
                    if (event.type == sf::Event::Closed)
                        window.close();
                }



                CoutArea(area);

                time_t timer, tmp_timer;
                GetTime(timer,tmp_timer,Speed);
                while(timer<tmp_timer)
                {
                    if(kbhit()  || sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
                    {
                        char get_key=0;
                        if (kbhit()) //fiz infinity wait keyboard from cmd
                            get_key=getch();
                        if (get_key == 32  || sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) //space
                        {
                            RotateTile();
                            RotateTile();
                            RotateTile();
                            if (!CheckBarrierDown(copyarea,y,x))
                            {
                                if (Tile_rotate == 8)
                                    Tile_rotate =1; //set previous rotation
                                else
                                    Tile_rotate *=2; //set previous rotation
                            }
                            else
                                block_music.play();


                            CopyCleanArea(area,copyarea);
                            IncreaseArea(area,y,x);
                            system("cls");
                            CoutArea(area);
                            GuiCout(area,window);
                            window.display();
                            MYdelay(100);
                        }
                        else if (get_key == 100  || sf::Keyboard::isKeyPressed(sf::Keyboard::D)) //right
                            {
                                if(CheckBarrierLeftRight(2,copyarea,y,x))
                                {
                                    if (x<(area_x-CountTileYElements()))
                                        x++;
                                    else
                                    {
                                        if(CheckBarrierLeftRight(2,copyarea,y,-1))
                                        x=0;
                                    }

                                    CopyCleanArea(area,copyarea);
                                    if(y>0) //fix bag then tile appear uper then area
                                    IncreaseArea(area,y,x);
                                    system("cls");
                                    CoutArea(area);
                                    GuiCout(area,window);
                                    window.display();
                                    block_music.play();
                                }
                                MYdelay(50);
                            }
                        else if (get_key == 115 || sf::Keyboard::isKeyPressed(sf::Keyboard::S)) //down
                            {
                                if (CheckBarrierDown(copyarea,y,x))
                                    y++;

                                CopyCleanArea(area,copyarea);
                                IncreaseArea(area,y,x);
                                system("cls");
                                CoutArea(area);
                                GuiCout(area,window);
                                window.display();
                                block_music.play();
                                MYdelay(50);
                            }
                        else if (get_key == 97 || sf::Keyboard::isKeyPressed(sf::Keyboard::A)) //left
                            {
                                if(CheckBarrierLeftRight(1,copyarea,y,x))
                                {
                                    if (x>0)
                                        x--;
                                    else
                                    {
                                        int tmp_x=(area_x-CountTileYElements());
                                        if(CheckBarrierLeftRight(1,copyarea,y,tmp_x+1))
                                        x=(area_x-CountTileYElements());
                                    }

                                    CopyCleanArea(area,copyarea);
                                    if(y>0) //fix bag then tile appear uper then area
                                    IncreaseArea(area,y,x);
                                    system("cls");
                                    CoutArea(area);
                                    GuiCout(area,window);
                                    window.display();
                                    block_music.play();
                                }
                                MYdelay(50);
                            }
                    }
                    GetTime(timer);

                }

/*downhil*/
                if (CheckBarrierDown(copyarea,y,x))
                {
                    y++;

                    CopyCleanArea(area,copyarea);
                    IncreaseArea(area,y,x);
                    system("cls");
                    CoutArea(area);
                    GuiCout(area,window);
                    window.display();
                    block_music.play();
                }
                else
                {
                    score +=20;
                    y=0;
                    srand (time(NULL)); //fica ot Tomasa :D
                                        //stob nekesirovalos
                    x=rand()%(area_x-4)+1;

                    GenerateNewTile();
                    CopyCleanArea(copyarea,area); //set new "good" area
                    Tile_rotate=8; //set 1x rotate

                    if(Find_and_DeleteLine(score,copyarea,area))
                        line_remove.play();
/*Game over*/
                    if(!CheckBarrierDown(copyarea,0,x))
                    {
                        game_music.stop();
                        game_over_music.play();
                        Texture game_over;
                        Sprite game_overr;
                        game_over.loadFromFile("pictures/game_over.png");
                        game_overr.setTexture(game_over);
                        game_overr.setPosition(0,0);
                        window.draw(game_overr);
                        window.display();

                        ifstream file("score.txt");
                        int str;
                        file >> str;
                        file.close();
                        if (str<score) //check previous score
                        {
                            //save score in file
                            ofstream file("score.txt");
                            file << score;
                            file.close();
                        }
                        Sleep(5000);
                        goto begin_game;
                    }


                    if(Speed>10)
                    Speed -=2;    //increase speed
                    cout<<Speed<<endl;
                }
                ////////////////////////////////////
                system("cls");
                window.clear();
                GuiCout(area,window);
                window.display();
            }
exit:
    return 0;
}
